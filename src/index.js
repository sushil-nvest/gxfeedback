import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {FeedbackProvider} from './ContextApi'

ReactDOM.render(<FeedbackProvider><App /></FeedbackProvider>, document.getElementById('root'));

serviceWorker.unregister();

