import React from 'react'

const FeedButton = ({handleFunc, CustomClassName,title}) => {
    return (
            <div onClick={handleFunc} className={CustomClassName}> {title}</div>
    )
}

export default FeedButton