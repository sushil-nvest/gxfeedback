import React from 'react'
import Lottie from 'lottie-react-web'
import loading from '../../assets/JSON/loading.json'
import './loading.css'

const Loading = () => {
    return (
        <div style={{ height: "100vh", position: "relative", overflow: "hidden" }}>
            <div className="aniLoading">
                <Lottie
                    options={{
                        animationData: loading
                    }}
                />
            </div>
        </div>
    )
}

export default Loading