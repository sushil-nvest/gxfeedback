import React, { Suspense, lazy } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Error from './Components/Error/Error'
import Loading from './ReuseableComponent/Loading/Loading'

const AsyncHome = lazy(()=>import("./Components/Home/Index"));
const AsyncFeedback = lazy(()=>import("./Components/Feedback/Feedback"));
const AsyncSidebar = lazy(()=>import("./Components/Sidebar/Sidebar"));

function App() {
  return (
    <Suspense fallback={<Loading/>}>
      <BrowserRouter>
        <AsyncSidebar />
        <Switch>
          <Route path="/" exact activeClassName="active" component={AsyncHome} />
          <Route path="/feedback" activeClassName="active" exact component={AsyncFeedback} />
          <Route path="/home" activeClassName="active" exact component={AsyncHome} />
          <Route component={Error} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
