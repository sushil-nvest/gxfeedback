import React, { useState, useContext } from 'react'
import './ReportForm.css'
import { Feedback } from '../../ContextApi';
import Button from '../../ReuseableComponent/Button/Button'
import TitleDescriptionUpload from './TitleDescriptionUpload'

const NativeApplication = () => {
    const { toggle,nativeApplication,handleNativePlatform,getwebNativeAppName,submitReport,handleSelectNativeAppToggle, selectNativeAppToggle } = useContext(Feedback)
    
    const handleNativeCallBackFunc = (value)=>{
        getwebNativeAppName(value)
        handleSelectNativeAppToggle()
    }
    
    const [Android] = useState([
        { id: 1, title: "Haircuts", CustomClassName: "btn selectButton animated zoomIn faster   mt-2", func: () => handleNativeCallBackFunc("Haircuts")},
        { id: 2, title: "GXVault", CustomClassName: "btn selectButton animated zoomIn fast mt-2", func: () => handleNativeCallBackFunc("GXVault")},
        { id: 3, title: "GXNitrous", CustomClassName: "btn selectButton animated zoomIn slow mt-2", func: () => handleNativeCallBackFunc("GXNitrous")},
        { id: 4, title: "NvestHealth", CustomClassName: "btn selectButton animated zoomIn  mt-2", func: () => handleNativeCallBackFunc("NvestHealth")},
        { id: 5, title: "CoinSeller", CustomClassName: "btn selectButton animated zoomIn delay-1s mt-2", func: () => handleNativeCallBackFunc("CoinSeller")},
    ])
    const [ios] = useState([
        { id: 1, title: "Haircuts", CustomClassName: "btn selectButton animated bounceInRight faster   mt-2", func: () => handleNativeCallBackFunc("Haircuts")},
        { id: 2, title: "GXVault", CustomClassName: "btn selectButton animated bounceInRight fast mt-2", func: () => handleNativeCallBackFunc("GXVault")},
        { id: 3, title: "GXNitrous", CustomClassName: "btn selectButton animated bounceInRight slow mt-2", func: () => handleNativeCallBackFunc("GXNitrous")},
        { id: 4, title: "NvestHealth", CustomClassName: "btn selectButton animated bounceInRight slower mt-2", func: () => handleNativeCallBackFunc("NvestHealth")},
        { id: 5, title: "CoinSeller", CustomClassName: "btn selectButton animated bounceInRight delay-1s mt-2", func: () => handleNativeCallBackFunc("CoinSeller")},
    ])


    

    const nativeApplicationData = ()=>{
        if(nativeApplication === "Android"){
            return (
                <>
                    {Android.map(button => (
                    <Button
                        key={button.id}
                        handleFunc={button.func}
                        CustomClassName={button.CustomClassName}
                        title={button.title}
                    />
                ))}
                </>
            )
        }
        else if(nativeApplication === "IOS"){
            return (
                <>
                {ios.map(button => (
                    <Button
                        key={button.id}
                        handleFunc={button.func}
                        CustomClassName={button.CustomClassName}
                        title={button.title}
                    />
                ))}
                </>
            )
        }
    }
    return (
        <>{selectNativeAppToggle?
        <div className="reportForm" style={{ display: toggle ? "none" : "block" }}>
            <h3 className="textGradient animated bounceInLeft"><u><b>Select </b>Native Application <b>({nativeApplication})</b></u></h3>
            <div className="mt-1">
                {nativeApplicationData()}
            </div>
            <br/>
            <span className="animated zoomIn delay-1s d-flex align-items-center" style={{cursor:"pointer", color:"red",float:"right"}} onClick={handleNativePlatform}> <i className="fas fa-arrow-left mr-1"/>Go Back</span>
        </div>:<TitleDescriptionUpload handleFunc={handleSelectNativeAppToggle} handleSubmitProps={submitReport}/>}
        </>
    )
}

export default NativeApplication