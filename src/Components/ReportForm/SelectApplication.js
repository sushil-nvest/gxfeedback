import React, { useState, useContext } from 'react'
import './ReportForm.css'
import { Feedback } from '../../ContextApi';
import Button from '../../ReuseableComponent/Button/Button'
import NativeApplication from './NativeApplication';
import TitleDescriptionUpload from './TitleDescriptionUpload'

const SelectApplication = () => {
    const { toggle, platform, handleNativePlatform, GoBackToPlatform, handleNativeToggle,submitReport, getwebNativeAppName,webToggle,handleWebToggle } = useContext(Feedback)

const handleCallBack = (value)=>{
    handleWebToggle()
    getwebNativeAppName(value)
} 
    const [webData] = useState([
        { id: 1, title: "GXBroker", CustomClassName: "btn selectButton animated bounceInRight  fast mt-2", func: () => handleCallBack("GXBroker") },
        { id: 2, title: "GXNitrous", CustomClassName: "btn selectButton animated bounceInRight delay-1s  mt-2", func: () => handleCallBack("GXNitrous") },
        { id: 3, title: "Nvest", CustomClassName: "btn selectButton animated bounceInRight delay-1s slow mt-2", func: () => handleCallBack("Nvest") },
        { id: 4, title: "GxTransfer", CustomClassName: "btn selectButton animated bounceInRight delay-1s slower mt-2", func: () => handleCallBack("GxTransfer") },
    ])
    const [native] = useState([
        { id: 1, platformName: "Android", CustomClassName: "btn selectButton animated bounceInRight  faster mt-2", func: () => handleNativePlatform("Android") },
        { id: 2, platformName: "IOS", CustomClassName: "btn selectButton animated bounceInRight slow  mt-2", func: () => handleNativePlatform("IOS") }
    ])
    const [SmartContract] = useState([
        { id: 1, platformName: "GXToken Generation Contract", CustomClassName: "btn selectButton animated bounceInRight  fast mt-2", func: () => handleCallBack("GXTokenGeneration") },
        { id: 2, platformName: "SEFCoin Generation Contract", CustomClassName: "btn selectButton animated bounceInRight delay-1s  mt-2", func: () => handleCallBack("SEFCoinGeneration") },
        { id: 3, platformName: "SEFCoin Mining Contract", CustomClassName: "btn selectButton animated bounceInRight  slow mt-2", func: () => handleCallBack("SEFCoinMining") },
        { id: 4, platformName: "GXToken Staking Contract", CustomClassName: "btn selectButton animated bounceInRight slower  mt-2", func: () => handleCallBack("GXTokenStaking") },
    ])

    const platformSelection = () => {
        if (platform === "Website") {
            return (<>
                {webData.map(button => (
                    <Button
                        key={button.id}
                        handleFunc={button.func}
                        CustomClassName={button.CustomClassName}
                        title={button.title}
                    />
                ))}
            </>

            )
        }
        else if (platform === "Native") {
            return (
                <>
                    {native.map(button => (
                        <Button
                            key={button.id}
                            handleFunc={button.func}
                            CustomClassName={button.CustomClassName}
                            title={button.platformName}
                        />
                    ))}
                </>
            )
        }
        else if (platform === "SmartContract") {
            return (
                <>
                    {SmartContract.map(button => (
                        <Button
                            key={button.id}
                            handleFunc={button.func}
                            CustomClassName={button.CustomClassName}
                            title={button.platformName}
                        />
                    ))}
                </>
            )
        }
    }
    return (
        <>{webToggle?
            <>{handleNativeToggle ?
                <div className="reportForm" style={{ display: toggle ? "none" : "block" }}>
                    <h3 className="textGradient animated rollIn"><u><b>Select </b>{platform}</u></h3>
                    <div className="mt-1">
                        {platformSelection()}
                    </div>
                    <br />
                    <span className="animated zoomIn delay-1s d-flex align-items-center" style={{ cursor: "pointer", color: "red", float: "right" }} onClick={GoBackToPlatform}> <i className="fas fa-arrow-left mr-1" />Go Back</span>
                </div> : <NativeApplication />}
            </>:<TitleDescriptionUpload handleFunc={handleWebToggle} handleSubmitProps={submitReport}/>}
        </>
    )
}

export default SelectApplication