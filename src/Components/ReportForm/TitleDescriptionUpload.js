import React, { useContext} from 'react'
import './ReportForm.css'
import { Feedback } from '../../ContextApi';
import ReactStars from 'react-stars'
import UploadFile from './UploadFile'

import ThankYouModal from './ThankYouModal' 


const TitleDescriptionUpload = ({ handleFunc, handleSubmitProps }) => {
    const {handleTitleError,handleDescError, webNativeAppName, handleChange, title, description, ratingChanged, appRating, handleUploadFileToggle, uploadFileToggle,titleError,descError,rateError} = useContext(Feedback)
    
    var placeholdervalue = "Ex: Your feedback is important to us. "

    const isEnabled = title.length > 6 && description.length > 15 && appRating > 0



    return (
        <>{uploadFileToggle ?
            <form onSubmit={handleSubmitProps}>
                <div className="reportForm detailForm animated bounceInRight">
                    <h3 className="textGradient animated bounceInLeft"><u><b>Enter </b>detail for {webNativeAppName}      </u></h3>
<br/>
                    <div className="mt-1 animated bounceInRight " style={{ position: "relative" }}>

                        <span className="p-float-label animated bounceInRight">
                            <label>Enter Title</label>
                            <input onBlur={handleTitleError}  name="title" onChange={handleChange} placeholder="Ex: App is broken" className="form-control EmailInput mb-2" value={title} required style={{borderBottom:titleError?"2px solid red":""}}/>
                            {titleError?<span className={titleError?"animated slideInDown":"animated slideOutUp"} style={{color:"red", backgroundColor:"#ffebee",padding:"4px", borderRadius:"4px"}}><i className="fas fa-exclamation-triangle"/>&nbsp; Title cannot be empty</span>:""}
                        </span>
                        <br />
                        <div className="p-float-label animated bounceInRight form-group" >
                            <label>Enter Description</label>
                            <textarea style={{borderBottom:descError?"2px solid red":""}} onBlur={handleDescError} rows="3" name="description" onChange={handleChange} placeholder={placeholdervalue} value={description} className="form-control EmailInput mb-2" required />
                            {descError?<span className={descError?"animated slideInDown":"animated slideOutUp"} style={{color:"red", backgroundColor:"#ffebee",padding:"4px", borderRadius:"4px"}}><i className="fas fa-exclamation-triangle"/>&nbsp; Description cannot be empty</span>:""}
                        </div>
                    </div>
                    <div className="p-float-label animated bounceInRight form-group" >
                        <label>Rating</label>
                        <ReactStars  count={5} onChange={ratingChanged} size={30} value={appRating} color2={'var(--defaultColor)'} />
                        
                    </div>
                    <ThankYouModal/>
                    <div className="d-flex justify-content-between animated zoomIn">
                        <div className="d-flex align-items-center">
                            <button disabled={!isEnabled} className="btn FeedBackButton mr-2">Submit</button>
                            <button disabled={!isEnabled} onClick={handleUploadFileToggle} type="button" style={{ textOverflow: "ellipsis", whiteSpace: "nowrap" }} className="btn FeedBackButton">Upload File</button>
                        </div>
                        <span className="animated zoomIn delay-1s d-flex align-items-center" style={{ cursor: "pointer", color: "red", float: "right" }} onClick={handleFunc}> <i className="fas fa-arrow-left mr-1" />Go Back</span>
                    </div>
                </div>
            </form> : <UploadFile />} 
        </>
    )
}

export default TitleDescriptionUpload