import React, { useContext } from 'react'
import { Feedback } from '../../ContextApi'
import ReactFileReader from "react-file-reader";
import ThankYouModal from './ThankYouModal' 


const UploadFile = () => {
    const { handleFiles, handleUploadFileToggleBack,submitReport,fileDetail,reportImages,uuid} = useContext(Feedback)


    var reportImageVal = reportImages.length

    const renderFiles =()=>{
        if( reportImageVal === 0){
            return (
                <>
                    <h3 className="m-0">upload file</h3>
                </>
            )
        }else{
            return (
                <>
                {reportImages.map((file,index)=>{
                    const datass = fileDetail.fileList
                    const filename =datass[index].name
                    const fileSliceFromEnd= filename.slice(-4)
                    const fileSliceFromStart= filename.slice(0,6)
                    const coinName = `${fileSliceFromStart}...${fileSliceFromEnd}`
                    return (
                        <div key={uuid+Math.random()}  className="d-flex align-items-center justify-content-between p-2 m-2" style={{backgroundColor:"#fff", borderRadius:"8px"}}>
                            <p className="d-flex m-0 mr-5"><i className="fas fa-photo-video mr-2"></i>{coinName}</p>
                            <i className="fas fa-check-circle" style={{ color: "#4caf50" }}></i>
                        </div>
                    )
                })}
                </>
            )
        }
    }


    return (
        <div>
            <ReactFileReader fileTypes={[".png", ".jpg"]} base64={true} multipleFiles={true} handleFiles={handleFiles}>
                <div className="p-2">
                        <div className="d-flex justify-content-between align-items-center">
                            <p className="m-0 mr-2">Uploaded <b>{reportImages.length}</b> files</p>
                            <button className="btn float-right p-0" style={{ color: "#117abf" }}>Upload<i className="fas fa-cloud-upload-alt ml-2"></i></button>
                        </div>
                    </div>
            </ReactFileReader>
            <div className="SSUpload">
                    <div className="p-2" style={{ backgroundColor: "#f6f6f6", borderRadius:"8px",overflowY:"auto", maxHeight:"300px" }}>
                        {renderFiles()}        
                    </div>
                </div>
                <div className="justify-content-between animated zoomIn d-flex align-items-center my-3">
                            <button onClick={submitReport} className="btn FeedBackButton mr-2">Submit</button>    
                        <span className="animated zoomIn delay-1s d-flex align-items-center" style={{ cursor: "pointer", color: "red", float: "right" }} onClick={handleUploadFileToggleBack}> <i className="fas fa-arrow-left mr-1" />Go Back</span>
                    </div>
                <ThankYouModal/>
        </div>
    )
}

export default UploadFile