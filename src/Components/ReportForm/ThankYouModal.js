import React, { useContext } from 'react'
import { Feedback } from '../../ContextApi';
import { Modal } from 'react-bootstrap'
import whitelogo from '../../assets/Images/gxwhite.png'

const ThankYouModal = () => {
    const { feedbackModal, handleSubmit } = useContext(Feedback)
    return (
        <Modal show={feedbackModal} style={{top:"20vh"}}>
            <Modal.Body className="p-0" style={{height:"99px",overflow:"hidden"}}>
            <div className="d-flex position-relative" style={{borderRdaius:"3px"}}>
                <div className="one"><img src={whitelogo} alt="gxlogo" className="img-fluid"/></div>
                <div className="two position-relative"><h6>Thank you for your valuable feedback </h6>
                <i className="position-absolute far fa-times-circle" onClick={handleSubmit} style={{top:"10px",right:"10px", color:"red", cursor:"pointer"}}></i>
                </div>
            </div>
            
            </Modal.Body>
        </Modal>
    )
}

export default ThankYouModal