import React, { useState, useContext } from 'react'
import './ReportForm.css'
import { Feedback } from '../../ContextApi';
import Button from '../../ReuseableComponent/Button/Button'
import SelectApplication from './SelectApplication'

const ReportForm = () => {
    const { toggle, handlePlatformValue,PlatformRemove,handlePlatformToggle } = useContext(Feedback)
    const [buttonData] = useState([
        { id: 1, title: "Web Application", CustomClassName: "btn selectButton animated bounceInRight   mt-2", func: () => handlePlatformValue("Website")},
        { id: 2, title: "Native Application", CustomClassName: "btn selectButton animated bounceInRight slow mt-2", func: () => handlePlatformValue("Native")},
        { id: 3, title: "Smart Contract", CustomClassName: "btn selectButton animated bounceInRight slower mt-2", func: () => handlePlatformValue("SmartContract")},
    ])
    return (
        <>{PlatformRemove?
        <div className="reportForm" style={{ display: toggle ? "none" : "block" }}>
            <h3 className="textGradient animated bounceInDown"><u><b>Select</b> Platform </u></h3>
            <div className="mt-1">
                {buttonData.map(button => (
                    <Button
                        key={button.id}
                        handleFunc={button.func}
                        CustomClassName={button.CustomClassName}
                        title={button.title}
                    />
                ))}
            </div>
            <br/>
            <span className="animated zoomIn delay-1s d-flex align-items-center" style={{cursor:"pointer", color:"red",float:"right"}} onClick={handlePlatformToggle}> <i className="fas fa-arrow-left mr-1"/>Go Back</span>
        </div>:<SelectApplication/>}
        </>
    )
}

export default ReportForm