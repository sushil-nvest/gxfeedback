import React from 'react'

const Error = () => {
    return (
        <div style={{position:"relative"}} className="viewHeight">
            <div  className="centering">
                <h1>404 Error</h1>
            </div>
        </div>
    )
}

export default Error