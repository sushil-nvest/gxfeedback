import React from 'react'
// import { Feedback } from '../../../ContextApi'

const CommentValue = ({userComment}) => {
    return (
        <>{userComment === undefined?<></>:
            <>
            <hr />
            <h6 className="mt-2"><b>Admin Comment</b></h6>
            {userComment.map(comment=>{
                return (
                <div key={Math.random()+Math.random()} style={{ backgroundColor: "#e3f2fd", borderRadius: "8px",marginTop:"8px" }}>
                    <p style={{ padding: "10px" }}>{comment}</p>
                </div>
                )
            })}
            </>}
        </>
    )
}

export default CommentValue