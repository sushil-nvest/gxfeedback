import React, { useContext } from 'react'
import { Feedback } from '../../../ContextApi'
import CommentValue from './CommentValue'

const AdminFeedBackReply = ({iData,commentProps}) => {
    const { admin, handleChange,comment,submitComment} = useContext(Feedback)
    
    return (
        <div className="mt-2 feedbackSearch animated fadeIn slow">
            <CommentValue userComment={commentProps}/>
            {admin ? "" :
                <div className="mt-2">
                    <h6><b>Add Comment</b></h6>
                    <form onSubmit={(e)=>submitComment(iData,e)}>
                        <textarea required onChange={handleChange} value={comment} name="comment" type="text" className="form-control EmailInput mt-2" placeholder="enter comment" />
                        <br />
                        <button className="btn FeedBackButton">Add Submit</button>
                    </form>
                </div>
            }
        </div>
    )
}

export default AdminFeedBackReply