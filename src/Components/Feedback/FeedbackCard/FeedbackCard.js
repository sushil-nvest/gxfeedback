import React, { useContext } from 'react'
import './FeedbackCard.css';
import { Feedback } from '../../../ContextApi'


const FeedbackCard = () => {
    const { reportDatas, HandleDetailData, handleDelete, admin } = useContext(Feedback)
    
    return (
        
            <div className="p-4 FeedbackHeight">
                {reportDatas.length === 0? <div className="noData animated slideInDown">No Data To Show</div> :
                    <>{
                        reportDatas.map(reportData => {
                        const data = reportData.description
                        var sliceDescription = data.slice(0, 60)
                        return (
                            
                            <label className="col" key={reportData.id}>
                            
                                <div style={{ cursor: "pointer" }} onClick={() => HandleDetailData(reportData)}  className="px-3 mt-2 middle">
                                
                                    <input type="radio" name="radio" className="form-control" />
                                    <div className="FeedbackCard row mt-4" style={{ position: "relative" }}>
                                        <div className="col p-3">
                                            <div className="CardTitle cardBox" style={{ overflowWrap: "break-word" }}>
                                                <h6 style={{ fontWeight: "700", width:"95%" }}>{reportData.title} </h6>
                                                <h6 style={{ color: "#757575" }}>{reportData.email}</h6>
                                            </div>
                                            <br />
                                            <p style={{ fontWeight: "lighter", color: "#9e9e9e", overflowWrap: "break-word" }}>{sliceDescription}...<span id="scrollButton" style={{cursor:"pointer",color:"var(--defaultColor)",fontWeight:"700"}}>Read More</span></p>
                                            <div className="d-flex justify-content-between customMedia">
                                                <p className="mt-3" style={{ color: "#117abf" }}>{reportData.reportImages.length} Attachment</p>
                                                <span className="d-flex">
                                                    <p className="mt-3" style={{ color: "#117abf" }}>{reportData.platform} :</p>
                                                    <p className="mt-3">&nbsp; {reportData.webNativeAppName}</p>
                                                </span>
                                            </div>
                                        </div>
                                        {admin?"":<i onClick={admin ? "" : () => handleDelete(reportData.id)} className="far fa-trash-alt deleteButton" style={{color:"red"}}></i>}
                                    </div>
                                </div>
                            </label>
                            
                        )
                    })
                    }</>
                }
            </div>
    )
}

export default FeedbackCard
