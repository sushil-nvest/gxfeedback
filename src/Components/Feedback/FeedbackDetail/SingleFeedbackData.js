import React,{useContext} from 'react';
import {Feedback} from '../../../ContextApi'

const SingleFeedbackData = () => {
    const {reportDatas} = useContext(Feedback)

    const datas = reportDatas[0]  
    return (
        <div>
            <>
                        <div className="animated fadeIn slow">
                            <div className="d-flex align-items-center">
                                <div className="userProfile customProfile"></div>
                                <div>
                                    <h5 className="title CardTitle" style={{ fontWeight: "700" }}>{datas.title}</h5>
                                    <p className="CardTitle" style={{ color: "#757575" }}>{datas.email}</p>
                                </div>
                            </div>
                            <div className="d-flex pt-3 justify-content-between">
                                <p className="mr-4"><b>Platform : </b> {datas.platform == "Native" ? `${datas.platform} (${datas.nativeApplication})` : datas.platform}</p>
                                <p style={{ color: "var(--defaultColor)" }}><b>Application : </b>{datas.webNativeAppName}</p>

                            </div>
                            <hr className="m-0" />
                            <div className="pt-2">
                                <p className="text-justify" style={{ color: "#757575" }}>{datas.description}</p>
                                <p className="mt-3" style={{ color: "var(--defaultColor)", fontWeight: "500" }} >{image == undefined ? 0 : image.length} Attachment</p>
                            </div>
                        </div>
                        <div>
                            <div className="row no-gutters">
                                {image == undefined ? "" :
                                    image.map((img, index) => {
                                        return (
                                            <div key={Math.random()} className="col-xs-12 p-2 col-sm-4 animated zoomIn faster" ><img className="img-fluid image" style={{ height: "100px" }} src={img} alt="image1" /></div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </>
        </div>
    )
}

export default SingleFeedbackData