import React, { useContext } from 'react'
import { Feedback } from '../../../ContextApi'
import AdminFeedBackReply from '../AdminFeedBackReply/AdminFeedBackReply'
import './FeedbackDetail.css'


const FeedbackDetail = () => {
    const { DetailData, reportDatas, detailShow } = useContext(Feedback)
    var image = DetailData.reportImages

    const { title, description, email, nativeApplication, webNativeAppName, platform, id, commentValue } = DetailData

    return (
        <div style={{ position: "relative", height: "100vh", overflow: "hidden" }} className={detailShow === true ? "emptyDetail" : ""}>
            <>
                <div className="col-md-8 p-4 customScreen" >
                    {reportDatas.length === 0 ? <div className="noData animated slideInUp">No Feedback</div> :
                        <>{detailShow ? <div className="noData animated slideInUp">Select Report</div> :
                            <div className="detailData">
                                <div>
                                    <div className="">
                                        {/* <div className="userProfile customProfile"></div> */}
                                        <div className="CardTitle" style={{ overflowWrap: "break-word" }}>
                                            <h5 className="" style={{ fontWeight: "700" }}>{title}</h5>
                                            <p className="" style={{ color: "#757575" }}>{email}</p>
                                        </div>
                                    </div>
                                    <div className="d-flex pt-3 justify-content-between">
                                        <p className="mr-4"><b>Platform : </b> {platform === "Native" ? `${platform} (${nativeApplication})` : platform}</p>
                                        <p style={{ color: "var(--defaultColor)" }}><b>Application : </b>{webNativeAppName}</p>

                                    </div>
                                    <hr className="m-0" />
                                    <div className="pt-2">
                                        <p className="text-justify" style={{ color: "#757575", overflowWrap: "break-word" }}>{description}</p>
                                        <p className="mt-3" style={{ color: "var(--defaultColor)", fontWeight: "500" }} >{image === undefined ? 0 : image.length} Attachment</p>
                                    </div>
                                </div>
                                <div>
                                    <div className="row no-gutters">
                                        {image === undefined ? "" :
                                            image.map((img, index) => {
                                                return (
                                                    <div key={Math.random()} className="col-xs-12 p-2 col-sm-4" ><img className="img-fluid image" style={{ height: "100px" }} src={img} alt="image1" /></div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                                <AdminFeedBackReply iData={id} commentProps={commentValue} />
                            </div>
                        }</>
                    }
                </div>
            </>
        </div>

    )
}

export default FeedbackDetail