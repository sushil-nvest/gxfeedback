import React, { useContext } from 'react'
import './feedback.css'
import SearchBox from './Searchbox/SearchBox'
import { Feedback } from '../../ContextApi'
import AsyncFeedbackCard from './FeedbackCard/FeedbackCard'
import AsyncFeedbackDetail from './FeedbackDetail/FeedbackDetail'



const FeedbackD = () => {
    const { handleLogin } = useContext(Feedback)
    return (
        <div className="FeedbackHome">
            <div className="row mx-0">
                <div className="col-md-5 p-2 UserFeedback  animated fadeIn">
                    <SearchBox />
                    <AsyncFeedbackCard />
                </div>
                <div className="col-md-7">
                    <AsyncFeedbackDetail />
                </div>
            </div>
            <span onClick={handleLogin} style={{ position: "absolute", top: 10, right: 10, color: "var(--defaultColor)", cursor: "pointer", fontWeight: "700" }}>Login</span>
        </div>
    )
}

export default FeedbackD