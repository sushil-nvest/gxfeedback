import React from 'react'
import './SearchBox.css'

const SearchBox = () => {
    return (
        <div className="p-4 feedbackSearch">
            <input type="text" placeholder="Search" className="form-control EmailInput"/>
        </div>
    )
}

export default SearchBox