import React, { useContext } from 'react'
import { Feedback } from '../../ContextApi'
import './home.css'
import CircleGradident from '../CustomStyle/CircleGradident/CircleGradident'
import CircleBorder from '../CustomStyle/CircleBorder/CircleBorder'
import ReportForm from '../ReportForm/SelectPlatform'

const Home = () => {
    const { handleChange, handleSubmit, email, toggle, mediaquery,error,handleValidation } = useContext(Feedback)
    const isEnabled = email.length > 10 && email.endsWith("@gmail.com")

    return (
        <div className="Home">
            <div className="row mx-0">
                <div className={mediaquery ? "col-md-6" : "col-md-6 mediaQuery"} style={{ position: "relative", height: "100vh", overflow: "hidden" }}>
                    <div className="HomeContent p-3">
                        <p className="my-0 animated slideInLeft" style={{ color: "var(--defaultColor)", fontWeight: 700 }}>Start the Change</p>
                        <p className="my-0 hedingStyle animated slideInLeft slow">Coordinate the</p>
                        <p className="my-0 hedingStyle animated slideInLeft">group thinking.</p>
                        <p className="animated slideInLeft slow" style={{ maxWidth: "400px", color: "#818181", padding: "8px", textAlign: "justify" }}>Your feedback is important to us. We will make sure, that your insights and feedback are attended and resolved at the earliest. If there’s anything else we can help you with, please let us know.
Your feedback is welcome and appreciated!</p>
                        <div className="row mx-0 inputSubmit animated slideInLeft slow">
                        <div className="p-float-label my-2">
                            <input autoComplete="off" required onBlur={handleValidation} id="eml" name="email" onChange={handleChange} type="email" className="mr-5 form-control EmailInput mb-2" placeholder="Enter your Email Id" style={{borderBottom:error?"2px solid red":""}} />
                            
                        </div>
                                <button onClick={handleSubmit} disabled={toggle ? !isEnabled : false} className="btn FeedBackButton">{toggle ? "Report or Feedback" : "Go Back"}</button>
                                {error?<span className={error?"animated slideInDown":"animated slideOutUp"} style={{color:"red", backgroundColor:"#ffebee",padding:"4px", borderRadius:"4px"}}><i className="fas fa-exclamation-triangle"/> This is not a valid email address and we only accept gmail account</span>:""}
                        </div>
                    </div>
                </div>
                <div className={mediaquery ? "col-md-6 p-3 formDiv mediaQuery" : "col-md-6 p-3 formDiv"} style={{ height: "100vh", position: "relative" }}>
                    <ReportForm />
                </div>
            </div>
            <div className="postioning">
                <CircleGradident />
            </div>

            <div className="postioningBorder">
                <CircleBorder />
            </div>
        </div>
    )
}

export default Home