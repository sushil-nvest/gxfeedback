import React from 'react'
import './Sidebar.css'
import { NavLink,withRouter } from 'react-router-dom'
import Logo from '../../assets/Images/GXBlue.png'

const Sidebar = () => {

    return (
        <div className="sidebar">
        <img src={Logo} alt="Gx" style={{width:"42px" ,marginLeft:"12px", marginTop:"12px",position:"absolute"}}/>
            <div className="sidebarWrapper">
                <div className="sidebarContent">
                    <NavLink to="/" exact>
                        <span className="sidebarLink my-4">Home</span>
                    </NavLink>
                    <NavLink to="/feedback" exact>
                        <span className="sidebarLink my-4">User Feedback</span>
                    </NavLink>
                </div>
            </div>

        </div>
    )
}

export default withRouter(Sidebar)