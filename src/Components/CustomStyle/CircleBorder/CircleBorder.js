import React from 'react';
import './CircleBorder.css'


const CircleBorder = () => {
    return (
            <div className="borderStyle animated slideInDown" >
                <div className="CircleBorderGradient" style={{ position: "relative" }}>
                    <div className="whiteCircle centering">
                    </div>
                </div>
            </div>
    )
}

export default CircleBorder
