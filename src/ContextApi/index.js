import React, { Component, createContext } from 'react'
import uuid from 'uuid/v1'
export const Feedback = createContext();

export class FeedbackProvider extends Component {
    state = {
        uuid: uuid(),
        toggle: true,
        email: '',
        mediaquery: true,
        platform: '',
        nativeApplication: '',
        PlatformRemove: true,
        handleNativeToggle: true,
        webNativeAppName: '',
        webToggle: true,
        title: '',
        description: '',
        appRating: null,
        reportImages: [],
        uploadFileToggle: true,
        reportDatas: [{
            id: "13ded960-2af4-11ea-a0e4-7128f07bd206",
            email: "sushilaadisharma@gmail.com",
            platform: "Website",
            webNativeAppName: "GXBroker",
            title: "Coordinate the  group thinking",
            description: "our feedback is important to us. We will make sure, that your insights and feedback are attended and resolved at the earliest. If there’s anything else we can help you with, please let us know. Your feedback is welcome and appreciated!",
            nativeApplication: "",
            appRating: 2.5,
            reportImages: [],
            commentValue: []
        }],
        fileDetail: [],
        feedbackModal: false,
        DetailData: [],
        admin: true,
        detailShow: true,
        comment: "",
        commentValue: [],
        formName: "comment",
        error:false,
        titleError:false,
        descError:false,
        rateError:false,
    }


    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleValidation=()=>{
        const {email} = this.state
        if(!email.endsWith("@gmail.com") || email === ''){
            this.setState({
                error:true
            })
        }
        else{
            this.setState({
                error:false
            })
        }
    }

    handleTitleError=()=>{
        const {title} = this.state
        if(title === ''){
            this.setState({
                titleError:true
            })
        }
        else{
            this.setState({
                titleError:false
            })
        }
    }
    handleDescError=()=>{
        const {description} = this.state
        if(description === ''){
            this.setState({
                descError:true
            })
        }else{
            this.setState({
                descError:false
            })
        }
    }
    
    handleSubmit = (e) => {
        this.setState({
            mediaquery: !this.state.mediaquery,
            toggle: !this.state.toggle,
            PlatformRemove: true,
            handleNativeToggle: true,
            webToggle: true,
            selectNativeAppToggle: true,
            reportImages: [],
            uploadFileToggle: true,
            feedbackModal: false,
            id: '',

        })
    }
    handlePlatformToggle = () => {
        this.setState({
            toggle: !this.state.toggle,
            mediaquery: true
        })
    }
    handlePlatformValue = (PlatformValue) => {
        this.setState({
            platform: PlatformValue,
            PlatformRemove: false
        })
    }
    handleSelectNativeAppToggle = () => {
        this.setState({
            selectNativeAppToggle: !this.state.selectNativeAppToggle,
            title: '',
            description: '',
            appRating: null
        })
    }

    handleNativePlatform = (nativeValue) => {
        this.setState({
            nativeApplication: nativeValue,
            handleNativeToggle: !this.state.handleNativeToggle
        })
    }

    //conditional next and previous
    GoBackToPlatform = () => {
        this.setState({
            PlatformRemove: true,
            webNativeAppName: '',
            platform: ''
        })
    }
    handlePlatformToggle = () => {
        this.setState({
            toggle: !this.state.toggle,
            mediaquery: true,
            platform: ''
        })
    }
    handleWebToggle = () => {
        this.setState({
            webToggle: !this.state.webToggle,
            title: '',
            description: '',
            appRating: null
        })
    }
    handleFeedbackModal = () => {
        this.setState({
            feedbackModal: false
        })
    }

    getwebNativeAppName = (webNativeAppNameValue) => {
        this.setState({
            webNativeAppName: webNativeAppNameValue
        })
    }
    handleUploadFileToggle = () => {
        this.setState({
            uploadFileToggle: false,
        })
    }
    handleUploadFileToggleBack = () => {
        this.setState({
            uploadFileToggle: true,
            reportImages: []
        })
    }

    ratingChanged = (newRating) => {
        this.setState({
            appRating: newRating
        })
    }
    handleFiles = files => {
        var data = files.base64;
        this.setState({
            reportImages: data,
            fileDetail: files
        })
    };
    HandleDetailData = (value) => {
        this.setState({
            DetailData: value,
            detailShow: false
        })
    }

    handleLogin = () => {
        this.setState({
            admin: !this.state.admin
        })
    }
    submitComment = (id, e) => {
        e.preventDefault()
        this.handleCommentValue(id)
    }



    submitReport = async (e) => {
        e.preventDefault()
        const reportData = {
            id: uuid(),
            email: this.state.email,
            platform: this.state.platform,
            webNativeAppName: this.state.webNativeAppName,
            title: this.state.title,
            description: this.state.description,
            nativeApplication: this.state.nativeApplication,
            appRating: this.state.appRating,
            reportImages: this.state.reportImages,
            commentValue: []
        }

        this.setState({
            feedbackModal: true,
            reportDatas: [...this.state.reportDatas, reportData]
        })
    }


    handleCommentValue = async (id) => {
        const updateComment = await this.state.reportDatas.map(c => {
            if (c.id === id) {
                return {
                    ...c,
                    commentValue: [...c.commentValue, this.state.comment],
                };
            } else {
                return c;
            }
        });

        const dComment = await updateComment.filter(c => c.id === id);
        this.setState({
            reportDatas: updateComment,
            comment: "",
            DetailData: dComment[0]
        });
    }
    handleDelete = async (id) => {
        const deletData = this.state.reportDatas.filter(data => {
            return data.id !== id
        })
        this.setState({
            reportDatas: deletData,
        })

    }


    render() {
        console.log("reportDatas", this.state.reportDatas)
        return (
            <Feedback.Provider value={{
                ...this.state,
                handleChange: this.handleChange,
                handleSubmit: this.handleSubmit,
                handlePlatformValue: this.handlePlatformValue,
                handleNativePlatform: this.handleNativePlatform,
                GoBackToPlatform: this.GoBackToPlatform,
                handlePlatformToggle: this.handlePlatformToggle,
                getwebNativeAppName: this.getwebNativeAppName,
                handleWebToggle: this.handleWebToggle,
                handleSelectNativeAppToggle: this.handleSelectNativeAppToggle,
                submitReport: this.submitReport,
                ratingChanged: this.ratingChanged,
                handleFiles: this.handleFiles,
                handleUploadFileToggle: this.handleUploadFileToggle,
                handleUploadFileToggleBack: this.handleUploadFileToggleBack,
                HandleDetailData: this.HandleDetailData,
                handleLogin: this.handleLogin,
                submitComment: this.submitComment,
                handleCommentValue: this.handleCommentValue,
                handleDelete: this.handleDelete,
                handleValidation:this.handleValidation,
                handleTitleError:this.handleTitleError,
                handleDescError:this.handleDescError,
                handleRateError:this.handleRateError

            }}>
                {this.props.children}
            </Feedback.Provider>
        )
    }
}

